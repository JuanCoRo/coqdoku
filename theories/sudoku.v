(***************************************************************************)
(* This file contains the formalization of what is a sudoku game, that is: *)
(*   - A grid of n x m endowed with the lexicographical order              *)
(*   - The set of usual rules and conditions                               *)
(***************************************************************************)

Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import all_ssreflect all_algebra.


Section sudoku.

(* A sudoku board is a n x n matrix of natural numbers.                       *)
(*                                                                            *)
(* We only formalize the algorithm for regular sudokus. That is, for sudokus  *)
(* consisting of n^2 cells and n different values.                            *)
(*                                                                            *)
(* Since a sudoku board can be seen as a matrix, we use mathcomp's definition *)
(* of matrices and adapt it to our needs.                                     *)
(* https://github.com/math-comp/math-comp/blob/master/mathcomp/algebra/matrix.v *)

(* The formula for regular sudoku boards is: Lem m denote the width and height *)
(* of a sudoku subboard. Then the width and height of a sudoku board is m^2.   *)
(* In the following table there are a few examples.                            *)
(*                                                                             *)
(*         +-----------------+-----------------+-----------------+             *)
(*         | Subboard Width  | Board Width and | Number of cells |             *)
(*         |   and Height m  |   Height m^2    |  on the board   |             *)
(*         +-----------------+-----------------+-----------------+             *)
(*         |        2        |        4        |       16        |             *)
(*         +-----------------+-----------------+-----------------+             *)
(*         |        3        |        9        |       81        |             *)
(*         +-----------------+-----------------+-----------------+             *)
(*         |        4        |       16        |       256       |             *)
(*         +-----------------+-----------------+-----------------+             *)
(*         |        5        |       25        |       625       |             *)
(*         +-----------------+-----------------+-----------------+             *)
(*         |        6        |       36        |      1296       |             *)
(*         +-----------------+-----------------+-----------------+             *)

Variable d : unit.
Variable n : nat.

(* Given n : nat, a board is a n.+1^2 x n.+1^2 matrix with coefficients in the *)
(* range {0, 1, 2, ..., (n.+1^2).-1}.The value None represents the empty cell. *)
(* We use n.+1 to avoid the degenerate case of the empty board.                *)

Definition board := 'M[option 'I_(n.+1^2)]_(n.+1^2).

Lemma sqrn_succ : exists m, n.+1^2 = m.+1.
Proof.
  case: n => [|m]; first by exists 0.
  rewrite -addn1 sqrnD addnCAC addnAC addn1.
  by exists (2 * (m.+1 * 1) + m.+1 ^ 2).
Qed.

Variable B : board.

Definition is_cell_value c := [exists i, exists j, B i j == c].

(* Definition is_cell_value' c := c \in [set i | i \in [set j | B i j == c]]. *)

Definition is_value_of_row i c := [exists j, B i j == c].

Definition is_value_of_col j c := [exists i, B i j == c].

Definition board_values := [set c | is_cell_value c].

Definition row_values i := [set c | is_value_of_row i c].

Definition col_values j := [set c | is_value_of_col j c].

(* The type of the board boxes, with the lexicographic enumeration *)
(* TODO:                                                                          *)
(* Maybe it makes no sense to have it lexicographically ordered, check this later *)
Definition box := 'I_n *lexi[d] 'I_n.

(* [is_box_coord i j b] evaluates to true if the cell (i, j) is in the box b *)
Definition is_box_coord (i j : 'I_(n.+1 ^ 2)) (b : box) :=
  match b with (a, b) => (i %% n == a - 1) && (j %% n == b - 1) end.

Definition is_box_value b c :=
  [exists i, exists j, (is_box_coord i j b) && (B i j == c)].

Definition box_values b := [set c | is_box_value b c].

(** SUDOKU RULES *)

(* The set of values which are non-empty *)
Definition solved_values := [set Some m | m in 'I_(n.+1 ^ 2)].
(* [set: 'I_(n.+1 ^ 2).+1] :\ (ord0). *)
(* [set x in 'I_(n.+1 ^ 2).+1 | 0 < x]. *)

(** Rows *)

(* Row condition for a row i: for any two cells in row i, if one is non-empty, *)
(* their values must be different                                              *)
Definition row_condition i :=
  [forall j1, forall j2 ,
        (j1 != j2) ==> ((B i j1 \in solved_values) ==> (B i j1 != B i j2))].
Definition row_rule := [forall i, row_condition i].

(* A solved row must contain all the numbers {0, ... , (n.+1^2).-1} *)
Definition row_solved i := row_values i == solved_values.
Definition solved_rows := [forall i, row_solved i].

(** Columns *)

(* Col condition for a col i: for any two cells in col i, if one is non-empty, *)
(* their values must be different                                              *)
Definition col_condition i :=
  [forall j1, forall j2 ,
        (j1 != j2) ==> ((B j1 i \in solved_values) ==> (B j1 i != B j2 i))].
Definition col_rule := [forall i, col_condition i].


(* A solved column must contain all the numbers {0, ... , (n.+1^2).-1} *)
Definition col_solved j := col_values j == solved_values.
Definition solved_cols := [forall j, col_solved j].

(** Boxes *)

(* Box condition for a box i: for any two cells in box i, if one is non-empty, *)
(* their values must be different                                              *)
Definition box_condition b :=
  [forall i1, forall i2, forall j1, forall j2 ,
        (is_box_coord i1 j1 b) && (is_box_coord i2 j2 b) ==>
        ((i1, j1) != (i2, j2)) ==>
        ((B i1 j1 \in solved_values) ==> (B i1 j1 != B i2 j2))].
Definition box_rule := [forall b, box_condition b].

(* A solved box must contain all the numbers {0, ... , (n.+1^2).-1} *)
Definition box_solved b := box_values b == solved_values.
Definition solved_boxes := [forall b, box_solved b].

(* Enforce the conditions on a board *)
Definition sudoku_board := [&& row_rule, col_rule & box_rule].

(* A solved sudoku is a sudoku with no empty cells *)
Definition sudoku_solved := [&& solved_rows, solved_cols & solved_boxes].

Lemma sudoku_solved_is_board : sudoku_solved -> sudoku_board.
Proof.
  rewrite /sudoku_solved /sudoku_board /solved_rows /solved_cols /solved_boxes.
  rewrite /row_solved /col_solved /box_solved /row_rule /col_rule /box_rule.
  rewrite /row_condition /col_condition /box_condition.
  rewrite /row_values /col_values /box_values /solved_values.
  move/and3P=> [/forallP row_val /forallP col_val /forallP box_val].
  apply/and3P; split.
  1-2: apply/forallP=> i; apply/forallP=> j1; apply/forallP=> j2.
  1-2: apply/implyP=> neq_j1j2; apply/implyP=> B_sol.
  - apply/implyP=> /eqP B_eq; have := row_val i. move/eqP=> seteq.
    (* have: B i j1 \in [set c | is_value_of_row i c]. *)
    (*   by rewrite inE /is_value_of_row; apply/existsP; exists j1. *)
    have stuff: B i j2 \in [set c | is_value_of_row i c].
      by rewrite inE /is_value_of_row; apply/existsP; exists j2.
    have pre_finj: forall ord, {j | ord = B i j}.
      admit.
    pose f := fun ord => match pre_finj ord with exist j _ => j end.
    have fin : injective f.
      (* move=> x1 x2; case cx1: x1=> [y1|]; case cx2: x2=> [y2|]. *)
      admit.
    have fE: forall j, f (B i j) = j.
      move=> j.
      rewrite /f; case: pre_finj {f fin}.
      admit.
    have /= := (imset_f f B_sol). rewrite fE.
    pose ff := fun ord i j (prf : ord == B i j) => j.

    (* have := (imset_f ff B_sol). *)
    (* move/imsetP: B_sol. case.  *)

    (* rewrite !seteq. *)
    (* move=> /eqP. move=> /(congr1 card). *)

    (* have := row_val i. *)

    (* apply/(@implyP (B i j1 \in solved_values) (B i j1 != B i j1)). => B_sol. *)
Admitted.

Lemma solved_values_uniq : uniq (enum solved_values).
Proof. by apply/enum_uniq. Qed.

Lemma solved_values_card : #|solved_values| = (n.+1 ^ 2).
Proof. by rewrite /solved_values card_imset ?card_ord//; apply/Some_inj. Qed.
  (* rewrite /solved_values cardsD /=. *)
  (* have -> : [set: 'I_(n.+1 ^ 2).+1] :&: [set ord0] = [set ord0]. *)
  (*   by apply/setP => x; rewrite !inE. *)
  (* by rewrite cardsE card_ord cards1 subn1 -pred_Sn. *)
  (* (* have : [set ord0] \subset [set: 'I_(n.+1 ^ 2).+1]. *) *)
  (* (*   by apply/subsetP=> x; rewrite inE=> /eqP ->. *) *)

(* TODO: Generalize so the size hypothesis can be removed *)
Lemma nuniq_neq {T: eqType} (t : T) (s1 s2 : seq T) :
  size s1 == size s2 -> uniq s1 -> ~~uniq s2 -> s1 != s2.
Proof.
  move=> /eqP eq_size s1u  /[dup]/uniqPn s2pn s2u.
  have [i [j [lt_ij lt_js /eqP nth_eq]]] := s2pn t.
  apply/eqP=> /(congr1 (nth t)) fls.
  rewrite -fls in nth_eq; rewrite -eq_size in lt_js.
  have fls2 := nth_uniq t (ltn_trans lt_ij lt_js) lt_js s1u.
  rewrite fls2 in nth_eq; move/eqP: nth_eq lt_ij=> ->.
  by rewrite ltnn.
Qed.

(* A consequence of this rules is that no row, column or box has *)
(* any repeated number.                                          *)

Lemma row_norep : sudoku_board ->
  [forall i, ~~[exists j1, exists j2, [&& B i j1 == B i j2,
    B i j1 \in solved_values,
    B i j2 \in solved_values &
    j1 != j2]]].
Proof.
  move/and3P=> [/forallP rr _ _].
  apply/forallP=> i; apply/existsPn=> [j1]; apply/existsPn=> [j2].
  rewrite /solved_values.
  apply/negP=> /and4P[/eqP eqf solved_r solved_c j_diff].
  have := rr i; rewrite /row_condition=> /forallP /(_ j1) /forallP /(_ j2).
  by rewrite j_diff solved_r eqf !implyTb eqxx.
Qed.

Lemma col_norep : sudoku_board ->
  [forall i, ~~[exists j1, exists j2, [&& B j1 i == B j2 i,
    B j1 i \in solved_values,
    B j2 i \in solved_values &
    j1 != j2]]].
Proof.
  move/and3P=> [ _ /forallP cr _].
  apply/forallP=> i; apply/existsPn=> [j1]; apply/existsPn=> [j2].
  rewrite /solved_values.
  apply/negP=> /and4P[/eqP eqf solved_r solved_c j_diff].
  have := cr i; rewrite /row_condition=> /forallP /(_ j1) /forallP /(_ j2).
  by rewrite j_diff solved_r eqf !implyTb eqxx.
Qed.

Lemma box_norep : sudoku_board ->
  [forall b, forall i1, forall j1,
    is_box_coord i1 j1 b ==>
    ~~[exists i2, exists j2, [&& is_box_coord i2 j2 b,
                    B i1 j1 == B i2 j2,
                    B i1 j1 \in solved_values,
                    B i2 j2 \in solved_values,
                    i1 != i2 & j1 != j2]]].
Proof.
  move/and3P=> [_ _ /forallP bc].
  apply/forallP=> b; apply/forallP=> i1; apply/forallP=> j1; apply/implyP=> bc1.
  apply/existsPn=> [i2]; apply/existsPn=> [j2].
  rewrite /solved_values.
  apply/negP=> /and5P[ bc2 /eqP eqf solved_r solved_c /andP[i_diff j_diff]].
  have := bc b; rewrite /box_condition.
  move=> /forallP /(_ i1) /forallP /(_ i2) /forallP /(_ j1) /forallP /(_ j2).
  have ij_diff : (i1, j1) != (i2, j2).
    rewrite /negb; case: ifP=> // /eqP /(congr1 fst) /= fls.
    by move: i_diff; rewrite fls eqxx.
  by rewrite bc1 bc2 ij_diff solved_r eqf !implyTb eqxx.
Qed.

End sudoku.

