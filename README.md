<!---
This file was generated from `meta.yml`, please do not edit manually.
Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.
--->
# Coqdoku

[![GitLab CI][pipeline-shield]][pipeline-link]
[![coqdoc][coqdoc-shield]][coqdoc-link]

[pipeline-shield]: https://img.shields.io/gitlab/pipeline/JuanCoRo/coqdoku/main
[pipeline-link]: https://gitlab.com/JuanCoRo/coqdoku/-/commits/main


[coqdoc-shield]: https://img.shields.io/badge/docs-coqdoc-blue.svg
[coqdoc-link]: https://JuanCoRo.gitlab.io/coqdoku/coqdoku.sudoku.html


A sudoku game formally verified in the [Coq](https://coq.inria.fr/) proof assistant, powered with the [Coq/SSReflect](https://coq.inria.fr/distrib/current/refman/proof-engine/ssreflect-proof-language.html) language. The formalization is based on the [Mathematical Components](https://math-comp.github.io/) library.


The project consists of two main parts:

 * Formalizing, implementing and extracting to [OCaml](https://ocaml.org/) a sudoku-solver algorithm.
 * Wrapping the extracted OCaml code to build a simple game with a verified sudoku-solver.


 ## Formalization structure

 We formalize the algorithm presented in the paper [A Pencil-and-Paper Algortihm for Solving Sudoku Puzzles](http://www.ams.org/notices/200904/rtx090400460p.pdf) by J. F. Crook.

 The formalization consists of all files in the `theories` folder, which are the following:

 * `sudoku.v`: formalization of the concept of a sudoku (i.e. a grid together with the usual rules).
 * `crook.v`: formalization and proof of termination of Crook's algorithm. (to be developed)
 * `solver.v`: implementation of Crook's algorithm, together with a correctness proof. (to be developed)
 * `extraction.v`: extraction of the algorithm to OCaml. (to be developed)

## Meta

- Author(s):
  - Juan Conejero (initial)
- License: [Mozilla Public License 2.0](LICENSE)
- Compatible Coq versions: 8.12 and 8.13
- Additional dependencies:
  - [MathComp](https://math-comp.github.io) 1.12.0 (`ssreflect` suffices)
- Coq namespace: `coqdoku`
- Related publication(s):
  - [A Pencil-and-Paper Algortihm for Solving Sudoku Puzzles](http://www.ams.org/notices/200904/rtx090400460p.pdf) 

## Building and installation instructions

Start by making sure the dependencies are installed. Then:
``` shell
git clone https://gitlab.com/JuanCoRo/coqdoku.git
cd coqdoku
make   # or make -j <number-of-cores-on-your-machine>
```


## HTML Documentation

To generate HTML documentation, run `make coqdoc` and point your browser at
`docs/coqdoc/toc.html`.

The documentation is also available
[online](https://JuanCoRo.gitlab.io/coqdoku/toc.html).
 
The documentation is generated with
[CoqdocJS](https://github.com/palmskog/coqdocjs).
