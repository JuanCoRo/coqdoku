---
# This file was generated from `meta.yml`, please do not edit manually.
# Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.
title: Coqdoku
lang: en
header-includes:
  - |
    <style type="text/css"> body {font-family: Arial, Helvetica; margin-left: 5em; font-size: large;} </style>
    <style type="text/css"> h1 {margin-left: 0em; padding: 0px; text-align: center} </style>
    <style type="text/css"> h2 {margin-left: 0em; padding: 0px; color: #580909} </style>
    <style type="text/css"> h3 {margin-left: 1em; padding: 0px; color: #C05001;} </style>
    <style type="text/css"> body { width: 1100px; margin-left: 30px; }</style>
---

<div style="text-align:left"><img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px">
<a href="https://gitlab.com/JuanCoRo/coqdoku">View the project on GitLab</a>
<img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px"></div>

## About

Welcome to the Coqdoku project website!

A sudoku game formally verified in the [Coq](https://coq.inria.fr/) proof assistant, powered with the [Coq/SSReflect](https://coq.inria.fr/distrib/current/refman/proof-engine/ssreflect-proof-language.html) language. The formalization is based on the [Mathematical Components](https://math-comp.github.io/) library.


The project consists of two main parts:

 * Formalizing, implementing and extracting to [OCaml](https://ocaml.org/) a sudoku-solver algorithm.
 * Wrapping the extracted OCaml code to build a simple game with a verified sudoku-solver.


 ## Formalization structure

 We formalize the algorithm presented in the paper [A Pencil-and-Paper Algortihm for Solving Sudoku Puzzles](http://www.ams.org/notices/200904/rtx090400460p.pdf) by J. F. Crook.

 The formalization consists of all files in the `theories` folder, which are the following:

 * `sudoku.v`: formalization of the concept of a sudoku (i.e. a grid together with the usual rules).
 * `crook.v`: formalization and proof of termination of Crook's algorithm. (to be developed)
 * `solver.v`: implementation of Crook's algorithm, together with a correctness proof. (to be developed)
 * `extraction.v`: extraction of the algorithm to OCaml. (to be developed)

This is an open source project, licensed under the Mozilla Public License 2.0.

## Get the code

The current stable release of Coqdoku can be [downloaded from GitLab](https://gitlab.com/JuanCoRo/coqdoku/-/releases).

## Documentation

The coqdoc presentation of the source files can be browsed [here](coqdoku.sudoku.html)

Related publications, if any, are listed below.

- [A Pencil-and-Paper Algortihm for Solving Sudoku Puzzles](http://www.ams.org/notices/200904/rtx090400460p.pdf) 

## Help and contact

- Report issues on [GitLab](https://gitlab.com/JuanCoRo/coqdoku/issues)

## Authors and contributors

- Juan Conejero

