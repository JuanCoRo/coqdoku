# This file was generated from `meta.yml`, please do not edit manually.
# Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.

opam-version: "2.0"
maintainer: "juanconrod"
version: "dev"

homepage: "https://gitlab.com/JuanCoRo/coqdoku"
dev-repo: "git+https://gitlab.com/JuanCoRo/coqdoku.git"
bug-reports: "https://gitlab.com/JuanCoRo/coqdoku/issues"
doc: "https://JuanCoRo.gitlab.io/coqdoku/"
license: "MPL-2.0"

synopsis: "A sudoku game formally verified in the Coq proof assistant"
description: """
A sudoku game formally verified in the [Coq](https://coq.inria.fr/) proof assistant, powered with the [Coq/SSReflect](https://coq.inria.fr/distrib/current/refman/proof-engine/ssreflect-proof-language.html) language. The formalization is based on the [Mathematical Components](https://math-comp.github.io/) library.


The project consists of two main parts:

 * Formalizing, implementing and extracting to [OCaml](https://ocaml.org/) a sudoku-solver algorithm.
 * Wrapping the extracted OCaml code to build a simple game with a verified sudoku-solver.


 ## Formalization structure

 We formalize the algorithm presented in the paper [A Pencil-and-Paper Algortihm for Solving Sudoku Puzzles](http://www.ams.org/notices/200904/rtx090400460p.pdf) by J. F. Crook.

 The formalization consists of all files in the `theories` folder, which are the following:

 * `sudoku.v`: formalization of the concept of a sudoku (i.e. a grid together with the usual rules).
 * `crook.v`: formalization and proof of termination of Crook's algorithm. (to be developed)
 * `solver.v`: implementation of Crook's algorithm, together with a correctness proof. (to be developed)
 * `extraction.v`: extraction of the algorithm to OCaml. (to be developed)"""

build: [make "-j%{jobs}%" ]
install: [make "install"]
depends: [
  "coq" {(>= "8.12" & < "8.14~")}
  "coq-mathcomp-ssreflect" {(>= "1.12" & < "1.13~")}
]

tags: [
  "category:Manuals"
  "keyword:sudoku"
  "keyword:program verification"
  "keyword:formal verification"
  "keyword:coq proof assistant"
  "logpath:coqdoku"
]
authors: [
  "Juan Conejero"
]
